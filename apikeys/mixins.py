from __future__ import annotations
from backend.mixins import VisitorMixin
from . import authenticate


class APIVisitorMixin(VisitorMixin):
    """
    Allow to use api keys to set visitor information
    """
    def set_visitor_info(self):
        # Try the default from VisitorMixin
        super().set_visitor_info()

        # If it failed, try again with API key
        if self.visitor is None:
            self.visitor = authenticate(self.request)
