from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from backend import const
from backend.unittest import PersonFixtureMixin


class PersonPageTestCase(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Create a test person and process
        cls.persons.create("test", status=const.STATUS_DC, cn="Test", sn="Test", email="testuser@debian.org")
        cls.processes.create("app", person=cls.persons.test, applying_for=const.STATUS_DM)

    def test_advocate_search(self):
        # Check that the new person is listed on the page
        client = self.make_test_client(None)
        response = client.get(reverse('wizard_advocate'))
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse('wizard_advocate'), data={"person": "Test"})
        self.assertEqual(response.status_code, 200)

        self.assertIsNone(response.context["people"])
        self.assertEqual(response.context["processes"], {
            self.processes.app: "/process/1/advocate",
        })
