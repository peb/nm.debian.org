from __future__ import annotations
from django import http
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.shortcuts import redirect
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django.views.generic import View
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
import backend.models as bmodels
import backend.const as const
from backend.mixins import VisitorMixin, VisitorTemplateView
import signon.models as smodels
import json
import datetime
import os
import time
import logging

log = logging.getLogger(__name__)


class DBExport(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request, *args, **kw):
        from backend.export import export_db

        exported = export_db(full=False)

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        res = http.HttpResponse(content_type="application/json")
        res["Content-Disposition"] = "attachment; filename=nm-mock.json"
        json.dump(exported, res, cls=Serializer, indent=1)
        return res


class SalsaExport(VisitorMixin, View):
    def get(self, request, *args, **kw):
        site = get_current_site(request)
        salsa_host = settings.SALSA_HOST

        if not self.visitor or not self.visitor.is_staff:
            for addr in settings.SALSA_EXPORT_ALLOW_IPS:
                log.info("Trying to match %s against %s", addr, request.META["REMOTE_ADDR"])
                if request.META["REMOTE_ADDR"] == addr:
                    break
            else:
                log.warning("Remote address %s does not match %s", request.META["REMOTE_ADDR"], salsa_host)
                raise PermissionDenied

        signed = request.GET.get("signed") is not None
        if signed:
            import jwcrypto.jws
            import jwcrypto.jwk
            from jwcrypto.common import json_encode

            key_json = getattr(settings, "SIGNON_KEY", None)
            if key_json is None:
                raise ImproperlyConfigured("Using salsa export but SIGNON_KEY not set")
            key = jwcrypto.jwk.JWK.from_json(key_json)

        persons = []
        for identity in (
                smodels.Identity.objects.filter(issuer="salsa")
                                        .select_related("person", "person__ldap_fields")):
            person = identity.person
            if person is None:
                continue
            elif person.is_dd:
                status = "debian_developer"
            elif person.status in (const.STATUS_DM, const.STATUS_DM_GA):
                status = "debian_maintainer"
            elif person.status in (const.STATUS_EMERITUS_DD,):
                status = "debian_emeritus"
            elif person.status in (const.STATUS_REMOVED_DD,):
                status = "debian_removed_dd"
            else:
                continue

            person_data = {
                "aud": salsa_host,
                "sub": identity.subject,
                "https://nm.debian.org/claims/debian_status": status,
                "profile": request.build_absolute_uri(person.get_absolute_url()),
            }
            if person.is_dd:
                person_data["email"] = f"{person.ldap_fields.uid}@debian.org"
            persons.append(person_data)

        exported = {
            "iss": f"https://{site.domain}",
            "exp": int(time.time()) + 3600 * 24 * 3,
            "https://nm.debian.org/claims/persons": persons,
        }

        if signed:
            token = jwcrypto.jws.JWS(json_encode(exported))
            token.add_signature(
                    key, None,
                    {"alg": "RS256"},
                    {"kid": key.thumbprint()})

            return http.HttpResponse(token.serialize(), content_type="application/json")
        else:
            res = http.HttpResponse(content_type="application/json")
            json.dump(exported, res, indent=1)
            return res


class Impersonate(View):
    def get(self, request, key=None, *args, **kw):
        visitor = request.user
        if not visitor.is_authenticated or not visitor.is_admin:
            raise PermissionDenied
        if key is None:
            del request.session["impersonate"]
            messages.add_message(request, messages.INFO, _("Impersonation canceled"))
        else:
            person = bmodels.Person.lookup_or_404(key)
            request.session["impersonate"] = person.lookup_key
            messages.info(request, _("Impersonating {}").format(person.lookup_key))

        url = request.GET.get("url", None)
        if url is None:
            return redirect('home')
        else:
            return redirect(url)


class MailboxStats(VisitorTemplateView):
    template_name = "restricted/mailbox-stats.html"
    require_visitor = "admin"

    def get_context_data(self, **kw):
        ctx = super(MailboxStats, self).get_context_data(**kw)

        try:
            with open(os.path.join(settings.DATA_DIR, 'mbox_stats.json'), "rt") as infd:
                stats = json.load(infd)
        except OSError:
            stats = {}

        for email, st in list(stats["emails"].items()):
            st["person"] = bmodels.Person.lookup_by_email(email)
            st["date_first_py"] = datetime.datetime.fromtimestamp(st["date_first"])
            st["date_last_py"] = datetime.datetime.fromtimestamp(st["date_last"])
            if "median" not in st or st["median"] is None:
                st["median_py"] = None
            else:
                st["median_py"] = datetime.timedelta(seconds=st["median"])
                st["median_hours"] = st["median_py"].seconds // 3600

        ctx.update(
            emails=sorted(stats["emails"].items()),
        )
        return ctx
