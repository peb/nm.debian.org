from __future__ import annotations
from unittest import mock
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BaseFixtureMixin
from backend import const
from signon import providers


@override_settings(SIGNON_PROVIDERS=[
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
    providers.Provider(name="salsa", label="Salsa", single_bind=True),
])
class TestAuthentication(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Create a test person
        cls.persons.create("dd", status=const.STATUS_DD_NU)
        cls.persons.create("dd1", status=const.STATUS_DD_NU)

        providers.providers_by_name = None

    def test_no_active_identities(self):
        client = self.make_test_client(None)
        response = client.get(reverse('signon_login'))
        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {})
        self.assertFalse(request.user.is_authenticated)

    def test_one_active_unbound_identity(self):
        self.identities.create("dd", issuer="debsso", subject="dd@debian.org", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.dd,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.dd,
        })
        self.assertFalse(request.user.is_authenticated)

    def test_one_active_bound_identity(self):
        self.identities.create("dd", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.dd,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.dd,
        })
        self.assertEqual(request.user, self.persons.dd)

    def test_conflicting_active_bound_identities(self):
        # Multiple active bound identities pointing to different people cause
        # failure to authenticate
        self.identities.create("dd", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create("dd1", person=self.persons.dd1, issuer="salsa", subject="1", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.dd,
                "salsa": self.identities.dd1,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon_login'))

        self.assertEqual(log.output, [
            'ERROR:signon.middleware:Conflicting person mapping: identities '
            '(dd:debsso:dd@debian.org, dd1:salsa:1) map to at least dd and dd1'
        ])

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.dd,
            "salsa": self.identities.dd1,
        })
        self.assertFalse(request.user.is_authenticated)

    def test_aligned_active_bound_identities(self):
        # Multiple active bound identities pointing to different people cause
        # failure to authenticate
        self.identities.create(
                "debsso", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", person=self.persons.dd, issuer="salsa", subject="1", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
            "salsa": self.identities.salsa,
        })
        self.assertEqual(request.user, self.persons.dd)

    def test_remove_user_on_identity_deactivated(self):
        client = self.make_test_client(None)

        self.identities.create(
                "salsa", person=self.persons.dd, issuer="salsa", subject="1", audit_skip=True)

        # Authenticate successfully with one active identity

        def _instantiate_identities1(_self, request):
            request.signon_identities = {
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities1):
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.user, self.persons.dd)

        # Deactivate identities

        def _instantiate_identities2(_self, request):
            request.signon_identities = {}

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities2):
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertFalse(request.user.is_authenticated)

    def test_remove_user_on_identity_conflict(self):
        client = self.make_test_client(None)

        self.identities.create(
                "debsso", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", person=self.persons.dd1, issuer="salsa", subject="1", audit_skip=True)

        # Authenticate successfully with one active identity

        def _instantiate_identities1(_self, request):
            request.signon_identities = {
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities1):
            response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.user, self.persons.dd1)

        # Add another conflicting identity, authentication should disappear

        def _instantiate_identities2(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities2):
            with self.assertLogs():
                response = client.get(reverse('signon_login'))

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertFalse(request.user.is_authenticated)

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_auto_bind(self):
        self.identities.create(
                "debsso", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", issuer="salsa", subject="1", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon_login'))

        self.assertEqual(log.output, [
            "INFO:signon.middleware:Dd <dd@example.org>: auto associated to -:salsa:1",
        ])

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
            "salsa": self.identities.salsa,
        })
        self.assertEqual(request.user, self.persons.dd)

        self.identities.debsso.refresh_from_db()
        self.assertEqual(self.identities.debsso.person, self.persons.dd)

        self.identities.salsa.refresh_from_db()
        self.assertEqual(self.identities.salsa.person, self.persons.dd)

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_multiple_salsa_accounts(self):
        # One bound debsso account
        self.identities.create(
                "debsso", person=self.persons.dd, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        # One bound salsa account
        self.identities.create(
                "salsa1", person=self.persons.dd, issuer="salsa", subject="1", audit_skip=True)
        # One unbound salsa account
        self.identities.create(
                "salsa2", issuer="salsa", subject="2", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa2,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon_login'))

        self.assertEqual(log.output, [
            "INFO:signon.middleware:Dd <dd@example.org>: skipping association to -:salsa:2 because dd:salsa:1 is already associated",
            'INFO:signon.middleware:Dd <dd@example.org>: logging out spurious identity -:salsa:2',
        ])

        self.assertEqual(response.status_code, 200)

        request = response.context["request"]
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
        })
        self.assertEqual(request.user, self.persons.dd)

        self.identities.debsso.refresh_from_db()
        self.assertEqual(self.identities.debsso.person, self.persons.dd)

        self.identities.salsa1.refresh_from_db()
        self.assertEqual(self.identities.salsa1.person, self.persons.dd)

        self.identities.salsa2.refresh_from_db()
        self.assertIsNone(self.identities.salsa2.person)
