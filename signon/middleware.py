from __future__ import annotations
from django.core.exceptions import MiddlewareNotUsed, ImproperlyConfigured
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.backends import ModelBackend
import logging

log = logging.getLogger(__name__)


class SignonAuthBackend(ModelBackend):
    pass


class SignonMiddleware:
    def __init__(self, get_response):
        self.providers = getattr(settings, "SIGNON_PROVIDERS", ())
        if not self.providers:
            raise MiddlewareNotUsed()
        self.get_response = get_response

    def _instantiate_identities(self, request):
        """
        Populate request.signon_identities
        """
        request.signon_identities = {}
        for provider in self.providers:
            identity = provider.identity_for_request(request)
            if identity is not None:
                request.signon_identities[provider.name] = identity

    def __call__(self, request):
        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The signon middleware requires the authentication middleware"
                " to be installed.  Edit your MIDDLEWARE setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the SignonMiddleware class.")

        # Set signon_identities for each identity found in the session
        self._instantiate_identities(request)

        # If some identities are bound to a person, log in as that person
        person = self._find_bound_person(request)
        if person is None:
            if request.user.is_authenticated:
                # If there are no active identities, then remove any existing
                # authenticated user
                self._remove_invalid_user(request)
        else:
            # If the user is already authenticated and that user is the user we are
            # getting passed in the headers, then the correct user is already
            # persisted in the session and we don't need to continue.
            if request.user.is_authenticated:
                if request.user.pk != person.pk:
                    # An authenticated user is associated with the request, but
                    # it does not match the authorized user in the header.
                    self._remove_invalid_user(request)
            else:
                # We are seeing this user for the first time in this session.
                # Set request.user and persist user in the session by logging the
                # user in.
                request.user = person
                auth.login(request, person, backend="signon.middleware.SignonAuthBackend")

        if getattr(settings, "SIGNON_AUTO_BIND", False) and request.user.is_authenticated:
            from backend.models import Person
            from signon.models import Identity
            bound = [x.issuer for x in request.signon_identities.values() if x.person is not None]
            if bound:
                for identity in list(request.signon_identities.values()):
                    provider = identity.get_provider()
                    if identity.person is None:
                        auto_associate = True

                        if provider.single_bind:
                            old = Identity.objects.filter(issuer=identity.issuer, person=request.user).first()
                            if old is not None:
                                log.info("%s: skipping association to %s because %s is already associated",
                                         request.user, identity, old)
                                auto_associate = False

                        if auto_associate:
                            log.info("%s: auto associated to %s", request.user, identity)
                            identity.person = request.user
                            identity.save(
                                audit_author=Person.objects.get_housekeeper(),
                                audit_notes=f"Auto associated while logged in with {', '.join(bound)}",
                            )
                        else:
                            log.info("%s: logging out spurious identity %s", request.user, identity)
                            provider = provider.bind(request)
                            provider.logout(identity)

        return self.get_response(request)

    def _remove_invalid_user(self, request):
        """
        Remove the current authenticated user in the request which is invalid
        but only if the user is authenticated via SignonMiddleware
        """
        from django.contrib.auth import load_backend
        try:
            stored_backend = load_backend(request.session.get(auth.BACKEND_SESSION_KEY, ''))
        except ImportError:
            # backend failed to load
            auth.logout(request)
        else:
            if isinstance(stored_backend, SignonAuthBackend):
                auth.logout(request)

    def _find_bound_person(self, request):
        """
        If request.signon_identities has some bound identities, return the
        person bound to them.

        Return None if there are identities bound to different people.
        """
        person = None
        for identity in request.signon_identities.values():
            # Skip unbound identities
            if identity.person_id is None:
                continue

            # Make sure that all the bound identities point at the same person
            if person is None:
                person = identity.person
            elif person.pk != identity.person_id:
                log.error("Conflicting person mapping: identities (%s) map to at least %s and %s",
                          ", ".join(str(x) for x in request.signon_identities.values()),
                          person.lookup_key, identity.person.lookup_key)
                return None

        return person
