# Handling of external signon services

This handles authentication against external providers, and mapping
authenticated identities to site users.

# `models.Identity`

Represents an identity in an external authentication providers.

`Identity.person` is a nullable foreign key, pointing to the site user that
logs in using this identity and provider. If it is set, the Identity is
considered *bound*, else it's *unbound*.

A user who signs in using an unbound Identity is still considered anonymous by
Django's authentication, and the site can define procedures for binding
identities to actual users.

# providers

Represent authentication providers and how to interact with them.
