from backend.permissions import PersonVisitorPermissions
from nm2.lib.permissions import Permission
from backend import const


class ProcessVisitorPermissions(PersonVisitorPermissions):
    __slots__ = ("process",)

    add_log = Permission(doc="the visitor can add to the process log")
    view_mbox = Permission(doc="the visitor can view the mailbox of this process")
    view_private_log = Permission(doc="the visitor can view this process private log entries")
    view_intent = Permission(doc="the visitor can view this process declaration of intent")
    proc_freeze = Permission(doc="the visitor can freeze this process")
    proc_unfreeze = Permission(doc="the visitor can unfreeze this process")
    proc_approve = Permission(doc="the visitor can approve this process")
    proc_unapprove = Permission(doc="the visitor can unapprove this process")
    proc_close = Permission(doc="the visitor can close this process")
    proc_pause = Permission(doc="the visitor can pause this process")
    proc_unpause = Permission(doc="the visitor can unpause this process")
    am_assign = Permission(doc="the visitor can assign an AM to this process")
    am_unassign = Permission(doc="the visitor can unassign the current AM of this process")

    def __init__(self, process, visitor):
        super().__init__(process.person, visitor)
        self.process = process
        process_frozen = self.process.frozen_by is not None
        process_approved = self.process.approved_by is not None

        process_has_am_ok = self.process.requirements.filter(type="am_ok").exists()
        current_am_assignment = self.process.current_am_assignment
        if current_am_assignment is not None:
            is_current_am = current_am_assignment.am.person == self.visitor
        else:
            is_current_am = False

        if not self.process.closed and self.visitor is not None and not self.visitor.pending:
            self.add_log = True

        if self.visitor is None:
            pass
        elif self.visitor.is_admin:
            self.view_mbox = True
            self.view_private_log = True
            if not self.process.closed:
                if not process_frozen:
                    self.proc_freeze = True
                    if process_has_am_ok:
                        if current_am_assignment:
                            self.am_unassign = True
                            if current_am_assignment.paused:
                                self.proc_unpause = True
                            else:
                                self.proc_pause = True
                        else:
                            self.am_assign = True
                elif process_approved:
                    self.proc_unapprove = True
                else:
                    self.proc_unfreeze = True
                    self.proc_approve = True
                if not self.process.closed:
                    self.proc_close = True
        elif self.visitor == self.person:
            self.view_mbox = True
            if not self.process.closed:
                self.proc_close = True
        elif self.visitor.is_am:
            self.view_mbox = True
        # TODO: advocates of this process can see the mailbox(?)
        # elif self.process.advocates.filter(pk=self.visitor.pk).exists():
        #    self.add("view_mbox")

        # The current AM can see fd comments in this process
        if is_current_am:
            self.fd_comments = True
            self.view_private_log = True
            if not process_frozen and not self.process.closed:
                self.am_unassign = True
                if current_am_assignment.paused:
                    self.proc_unpause = True
                else:
                    self.proc_pause = True

        # Declarations of intent for emeritus/removed processes are only posted
        # on -private, and must therefore be only visible to DDs
        if (self.process.applying_for not in (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD)
                or (self.visitor is not None and self.visitor.is_dd)
                or (self.visitor == self.person)):
            self.view_intent = True


class RequirementVisitorPermissions(ProcessVisitorPermissions):
    __slots__ = ("requirement",)

    edit_statements = Permission("the visitor can add statements to this requirement")
    req_approve = Permission("the visitor can approve this requirement")
    req_unapprove = Permission("the visitor can unapprove this requirement")

    def __init__(self, requirement, visitor):
        super(RequirementVisitorPermissions, self).__init__(
            requirement.process, visitor)
        self.requirement = requirement
        process_frozen = self.process.frozen_by is not None

        if self.visitor is None:
            pass
        elif self.visitor.is_admin:
            if not self.process.closed:
                if self.requirement.type != "keycheck":
                    self.edit_statements = True
                if self.requirement.approved_by:
                    self.req_unapprove = True
                else:
                    self.req_approve = True
        elif not process_frozen and not self.process.closed:
            if self.requirement.type == "intent":
                if self.visitor == self.person:
                    self.edit_statements = True
                if self.visitor.is_dd:
                    if self.requirement.approved_by:
                        self.req_unapprove = True
                    else:
                        self.req_approve = True
            elif self.requirement.type == "sc_dmup":
                if self.visitor == self.person:
                    self.edit_statements = True
                if self.visitor.is_dd:
                    if self.requirement.approved_by:
                        self.req_unapprove = True
                    else:
                        self.req_approve = True
            elif self.requirement.type == "advocate":
                if self.process.applying_for == const.STATUS_DC_GA:
                    if self.visitor.status in (const.STATUS_DM, const.STATUS_DM_GA,
                                               const.STATUS_DD_NU, const.STATUS_DD_U):
                        self.edit_statements = True
                elif self.process.applying_for == const.STATUS_DM:
                    if self.visitor.status in (const.STATUS_DD_NU, const.STATUS_DD_U):
                        self.edit_statements = True
                elif self.process.applying_for == const.STATUS_DM_GA:
                    if self.visitor == self.person or self.visitor.status in (const.STATUS_DD_NU, const.STATUS_DD_U):
                        self.edit_statements = True
                elif self.process.applying_for == const.STATUS_DD_NU:
                    if self.visitor.status in (const.STATUS_DD_NU, const.STATUS_DD_U):
                        self.edit_statements = True
                elif self.process.applying_for == const.STATUS_DD_U:
                    if self.visitor.status in (const.STATUS_DD_NU, const.STATUS_DD_U):
                        self.edit_statements = True
                if self.visitor.is_dd:
                    if self.requirement.approved_by:
                        self.req_unapprove = True
                    else:
                        self.req_approve = True
            elif self.requirement.type == "am_ok":
                current_am_assignment = self.process.current_am_assignment
                if current_am_assignment:
                    if current_am_assignment.am.person == self.visitor:
                        self.edit_statements = True
                    elif self.visitor.is_am:
                        if self.requirement.approved_by:
                            self.req_unapprove = True
                        else:
                            self.req_approve = True
