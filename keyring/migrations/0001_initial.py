# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-02-21 15:42
from __future__ import unicode_literals

import backend.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Key',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fpr', backend.fields.FingerprintField(max_length=40, unique=True, verbose_name='OpenPGP key fingerprint')),
                ('key', models.TextField(verbose_name='ASCII armored key material')),
                ('key_updated', models.DateTimeField(verbose_name='Datetime when the key material was downloaded')),
                ('check_sigs', models.TextField(blank=True, verbose_name='gpg --check-sigs results')),
                ('check_sigs_updated', models.DateTimeField(null=True, verbose_name='Datetime when the check_sigs data was computed')),
            ],
        ),
    ]
