from . import models as kmodels
from backend import models as bmodels
from django import http
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils.timezone import now
import re
import json
import datetime


class Serializer(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, "strftime"):
            return o.strftime("%s")
            # return o.strftime("%Y-%m-%d %H:%M:%S")
        return json.JSONEncoder.default(self, o)


def json_response(val, status_code=200):
    res = http.HttpResponse(content_type="application/json")
    res.status_code = status_code
    json.dump(val, res, cls=Serializer, indent=1)
    return res


class Keycheck(View):
    """
    Web-based keycheck.sh implementation
    """
    def get(self, request, fpr, *args, **kw):
        if not re.match(r"^[A-Fa-f0-9]{40}$", fpr):
            return http.HttpResponseBadRequest(b"Invalid fingerprint")
        try:
            key = kmodels.Key.objects.get_or_download(fpr)

            # Do not redownload more than once every 5 minutes
            if key.key_updated < now() - datetime.timedelta(minutes=5):
                key.update_key()

            key.update_check_sigs()

            res = {}
            kc = key.keycheck()
            uids = []
            k = {
                "fpr": kc.key.fpr,
                "errors": sorted(kc.errors),
                "uids": uids
            }
            res[kc.key.fpr] = k

            for ku in kc.uids:
                uids.append({
                    "name": ku.uid.name,
                    "errors": sorted(ku.errors),
                    "sigs_ok": [x[9] for x in ku.sigs_ok],
                    "sigs_no_key": len(ku.sigs_no_key),
                    "sigs_bad": len(ku.sigs_bad)
                })

            try:
                bf = bmodels.Fingerprint.objects.get(fpr=fpr)
                k["person_id"] = bf.person_id
                k["person"] = bf.person.fullname
            except ObjectDoesNotExist:
                k["person_id"] = None
                k["person"] = None
            except MultipleObjectsReturned:
                # Should never happen because of unique constraints
                raise

            return json_response(k)
        except RuntimeError as e:
            return json_response({
                "error": str(e)
            }, status_code=500)
