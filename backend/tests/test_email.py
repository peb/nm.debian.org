from __future__ import annotations
from typing import List
from django.test import TestCase
from backend.email import StoredEmail
import tempfile
import contextlib
import mailbox
import email.message
import email


class TestStoredEmail(TestCase):
    @contextlib.contextmanager
    def mbox(self, messages: List[str]):
        with tempfile.NamedTemporaryFile(suffix=".mbox") as tf:
            mb = mailbox.mbox(tf.name)
            try:
                for msg in messages:
                    mb.add(email.message_from_string(msg.strip()))
            finally:
                mb.close()
            yield tf.name

    def test_plain(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 28 Feb 2020 16:27:47 +0100
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 28 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-28T16:27:47+01:00",
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])

    def test_missing_date(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": None,
                "date_iso": None,
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])

    def test_missing_timezone(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 28 Feb 2020 16:27:47
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 28 Feb 2020 16:27:47",
                "date_iso": "2020-02-28T16:27:47+00:00",
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])
