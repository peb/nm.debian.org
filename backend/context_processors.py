import backend.const as c


def const(request):
    ctx = {"ALL_STATUS": c.ALL_STATUS}

    for s in c.ALL_STATUS:
        ctx[s.code] = s.tag

    for p in c.ALL_PROGRESS:
        ctx[p.code] = p.tag

    return ctx
